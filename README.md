# kafka-java

Prueba concepto de configuración e implementación de Apache Kafka con Java

# How to execute

1. Start zookeeper to manage your kafka cluster: `%KAFKA_HOME%\bin\windows\zookeeper-server-start.bat %KAFKA_HOME%\config\zookeeper.properties`
2. Run a kafka broker: `%KAFKA_HOME%\bin\windows\kafka-server-start.bat %KAFKA_HOME%\config\server.properties`
3. Create a kafka topic: `%KAFKA_HOME%\bin\windows\kafka-topics.bat --create --topic concept-test --zookeeper localhost:2181 --replication-factor 1 --partitions 1`
4. Run consumer: `%KAFKA_HOME%\bin\windows\kafka-console-consumer.bat --bootstrap-server localhost:9092 --from-beginning --topic concept-test`