package com.softlution.util;

public class Constants {
	public static final String KAFKA_BROKER_HOST = "localhost:9092";
	public static final String KAFKA_GROUP_ID = "kafka-sandbox";
	public static final String KAFKA_TOPIC = "concept-test";
}
