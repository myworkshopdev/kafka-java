package com.softlution.controller;

import java.util.List;

import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.softlution.consumer.TopicConsumer;
import com.softlution.util.Constants;

@RestController
public class KafkaController {

    private KafkaTemplate<String, String> template;
    private TopicConsumer topicConsumer;

    public KafkaController(KafkaTemplate<String, String> template, TopicConsumer topicConsumer) {
        this.template = template;
        this.topicConsumer = topicConsumer;
    }

    @GetMapping("/kafka/produce")
    public void produce(@RequestParam String message) {
        template.send(Constants.KAFKA_TOPIC, message);
    }

    @GetMapping("/kafka/messages")
    public List<String> getMessages() {
        return topicConsumer.getMessages();
    }
}
