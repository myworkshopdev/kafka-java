# Instalación
1. Descargar la última versión de Apache Kafka desde la página web: https://kafka.apache.org/downloads
* Descomprimir el fichero descargado en el directorio elegido, siendo el directorio de instalación.
* Crear la variable de entorno: KAFKA_HOME.
* Añadir la variable de entorno al PATH.

# Configuración básica
1. Configuración de Apache Zookeeper:
* Zookeeper tiene un fichero de configuración específico en directorio de instalación. %KAFKA_HOME%\config\zookeeper.properties
* Las propiedades básicas de configuración que se deben modificar en caso de ser necesario son:
`dataDir=C:/apache-kafka/zookeeper`
`clientPort=2181`
2. Configuración de un Kafka Broker:
* Kafka Broker tiene un fichero de configuración específico en directorio de instalación. %KAFKA_HOME%\config\server.properties
* Las propiedades básicas de configuración que se deben modificar en caso de ser necesario son:
`broker.id=0`
`listeners=PLAINTEXT://:9092`
`log.dirs=C:/apache-kafka/kafka-logs/broker_0`
`zookeeper.connect=localhost:2181`

# Ejecución
1. Ejecución de Apache Zookeeper: `%KAFKA_HOME%\bin\windows\zookeeper-server-start.bat %KAFKA_HOME%\config\zookeeper.properties`
2. Ejecución de un Kafka Broker/Server: `%KAFKA_HOME%\bin\windows\kafka-server-start.bat %KAFKA_HOME%\config\server.properties`

# Gestión de topics
1. Listar todos los topics: `%KAFKA_HOME%\bin\windows\kafka-topics.bat --list --zookeeper localhost:2181`
2. Crear un topic: `%KAFKA_HOME%\bin\windows\kafka-topics.bat --create --zookeeper localhost:2181 --replication-factor 1 --paritions 100 --topic <topicName>`
3. Borrar un topic: `%KAFKA_HOME%\bin\windows\kafka-topics.bat --delete --zookeeper localhost:2181 --topic <topicName>` 
Nota: para hacer efectiva la eliminación agregar la siguiente línea en %KAFKA_HOME%\config\server.properties
`delete.topic.enable=true`
4. Ver información del detalle de un topic: `%KAFKA_HOME%\bin\windows\kafka-topics.bat --describe --zookeeper localhost:2181 --topic <topicName>`
